package Recipe.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import Recipe.models.Main;

public interface MainRepository extends JpaRepository<Main, Long> {

}
