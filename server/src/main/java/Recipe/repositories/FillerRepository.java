package Recipe.repositories;
import Recipe.models.Filler;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FillerRepository extends JpaRepository<Filler, Long> {
}
