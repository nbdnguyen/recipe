package Recipe.repositories;
import Recipe.models.Side;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SideRepository extends JpaRepository<Side, Long>{
}
