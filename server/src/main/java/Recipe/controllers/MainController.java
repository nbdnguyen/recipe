package Recipe.controllers;

import Recipe.models.Main;
import Recipe.repositories.MainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/v1/main")
public class MainController {

    @Autowired
    private MainRepository mainRepository;

    @GetMapping()
    public ResponseEntity<List<Main>> getAllMains() {
        List<Main> mains = mainRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(mains, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Main> getMain(@PathVariable Long id){
        Main retMain = new Main();
        HttpStatus status;
        if(mainRepository.existsById(id)){
            status = HttpStatus.OK;
            retMain = mainRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(retMain, status);
    }
    @PostMapping
    public ResponseEntity<Main> addMain(@RequestBody Main main) {
        Main retMain = mainRepository.save(main);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(retMain, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Main> updateMain(@PathVariable Long id, @RequestBody Main main){
        Main retMain = new Main();
        HttpStatus status;
        if(!id.equals(main.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(retMain,status);
        }
        retMain = mainRepository.save(main);
        status = HttpStatus.OK;
        return new ResponseEntity<>(retMain, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteMain(@PathVariable Long id){
        HttpStatus status;
        // We first check if the Library exists, this saves some computing time.
        if(mainRepository.existsById(id)){
            status = HttpStatus.OK;
            mainRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}
