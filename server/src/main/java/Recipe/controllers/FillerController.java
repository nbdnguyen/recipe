package Recipe.controllers;

import Recipe.models.Filler;
import Recipe.repositories.FillerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/Filler")
public class FillerController {
    @Autowired
    private FillerRepository fillerRepository;

    @GetMapping()
    public ResponseEntity<List<Filler>> getAllFillers() {
        List<Filler> fillers = fillerRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(fillers, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Filler> getFiller(@PathVariable Long id){
        Filler retFiller = new Filler();
        HttpStatus status;
        if(fillerRepository.existsById(id)){
            status = HttpStatus.OK;
            retFiller = fillerRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(retFiller, status);
    }
    @PostMapping
    public ResponseEntity<Filler> addFiller(@RequestBody Filler filler) {
        Filler retFiller = fillerRepository.save(filler);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(retFiller, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Filler> updateFiller(@PathVariable Long id, @RequestBody Filler filler){
        Filler retFiller = new Filler();
        HttpStatus status;
        if(!id.equals(filler.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(retFiller,status);
        }
        retFiller = fillerRepository.save(filler);
        status = HttpStatus.OK;
        return new ResponseEntity<>(retFiller, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteFiller(@PathVariable Long id){
        HttpStatus status;
        if(fillerRepository.existsById(id)){
            status = HttpStatus.OK;
            fillerRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}
