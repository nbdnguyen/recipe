package Recipe.controllers;

import Recipe.models.Side;
import Recipe.repositories.SideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/side")
public class SideController {
    @Autowired
    private SideRepository sideRepository;

    @GetMapping()
    public ResponseEntity<List<Side>> getAllSides() {
        List<Side> sides = sideRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(sides, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Side> getSide(@PathVariable Long id){
        Side retSide = new Side();
        HttpStatus status;
        if(sideRepository.existsById(id)){
            status = HttpStatus.OK;
            retSide = sideRepository.findById(id).get();
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(retSide, status);
    }
    @PostMapping
    public ResponseEntity<Side> addSide(@RequestBody Side side) {
        Side retSide = sideRepository.save(side);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(retSide, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Side> updateSide(@PathVariable Long id, @RequestBody Side side){
        Side retSide = new Side();
        HttpStatus status;
        if(!id.equals(side.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(retSide,status);
        }
        retSide = sideRepository.save(side);
        status = HttpStatus.OK;
        return new ResponseEntity<>(retSide, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteSide(@PathVariable Long id){
        HttpStatus status;
        if(sideRepository.existsById(id)){
            status = HttpStatus.OK;
            sideRepository.deleteById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}
