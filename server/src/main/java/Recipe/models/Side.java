package Recipe.models;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
public class Side {
    public Side() {}

    public Side(Long sideId, String name) {
        this.sideId = sideId;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "side_id")
    private Long sideId;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return sideId;
    }

    public void setId(Long sideId) {
        this.sideId = sideId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
