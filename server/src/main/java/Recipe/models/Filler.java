package Recipe.models;
import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.sun.istack.NotNull;

@Entity
public class Filler {
    public Filler() {}

    public Filler(Long fillerId, String name) {
        this.fillerId = fillerId;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "filler_id")
    private Long fillerId;

    @Column(name = "name")
    private String name;

    public Long getId() {
        return fillerId;
    }

    public void setId(Long fillerId) {
        this.fillerId = fillerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
