import React from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  BrowserRouter,
  Switch,
} from "react-router-dom";
import LandingView from "./views/LandingView";
import RecipeView from "./views/RecipeView";

function App() {
  return (
    <BrowserRouter>
    <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/welcome"></Redirect>
          </Route>
          <Route exact path="/welcome" component={LandingView}></Route>
          <Route exact path="/recipe" component={RecipeView}></Route>
          <Route>
            <Redirect to="/welcome"></Redirect>
          </Route>
        </Switch>
    </Router>
</BrowserRouter>
  );
}

export default App;
