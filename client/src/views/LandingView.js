import React from "react";
import {useHistory} from "react-router-dom";

function SubmitForm() {
    const history = useHistory();
    history.push('/recipe');
}

function LandingView() {
    return (
        <div>
            <div>
                <h1>Welcome to the recipe app page!</h1>
            </div>
            <div display="hidden">
                <p>Test text</p>
            </div>

            <form class="form" onSubmit={SubmitForm}>
                <label>Choose a main:</label>
                <select class="main-select">
                    <option value="Beef">Beef</option>
                    <option value="Pork">Pork</option>
                    <option value="Vegetarian">Vegetarian</option>
                </select>
                <br/>
                <label>Choose a filler:</label>
                <select class="filler-select">
                    <option value="Potato">Potato</option>
                    <option value="Pasta">Pasta</option>
                    <option value="Rice">Rice</option>
                </select>
                <br/>
                <label>Choose sides:</label>
                <select class="sides-select">
                    <option value="Veggies">Veggies</option>
                </select>
                <br/>
                <div>
                    <input type="submit" value="Generate recipe!"></input>
                </div>
            </form>
        </div>
    )
}

export default LandingView;